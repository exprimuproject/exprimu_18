package org.expu.dao;

import org.expu.entities.Utilisateur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.data.domain.Pageable;


public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long>{
		
	@Query("select u from Utilisateur u where email like :x ")
	public Utilisateur chercherUtilisateur(@Param("x") String email);

	@Query("select u from Utilisateur u where username like :x ")
	Utilisateur findUtilisateurByUserName(@Param("x")String username);
	
	


	@Query("select u from Utilisateur u where id_utilisateur like :x ")
	Utilisateur findUtilisateurById(@Param("x")Long id);
	
	@Query("select u from Utilisateur u where  u.username like :x")
	public Page<Utilisateur> chercherUtilisateurs(@Param("x") String mc,Pageable pageable);
}
