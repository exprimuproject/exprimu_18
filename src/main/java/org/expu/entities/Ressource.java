package org.expu.entities;

import java.util.*;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
public class Ressource {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long idRessource;
		private String type;
		private String url;
		
	@OneToOne
		private Actualite actuRessource; 
	
	@ManyToMany
		private Collection<Contenu> listCont;
}
