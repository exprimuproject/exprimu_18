package org.expu.entities;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import org.expu.enums.RoleEnum;

import lombok.Data;

@Entity
@Data
@PrimaryKeyJoinColumn(name = "idAuteur")
public class Auteur extends Utilisateur {
	
	
	@ManyToMany
	private Collection<Utilisateur> listAbonne;
	
	@OneToMany(mappedBy="auteur", fetch=FetchType.LAZY)
	private Collection<Contenu> listContenu;

	public Auteur() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Auteur(String username, String password, String firstname, String lastname, Collection<RoleEnum> roles,
			String email, Date dateCreation, Date dateNaissance, String adresse, String description, String image) {
		super(username, password, firstname, lastname, roles, email, dateCreation, dateNaissance, adresse, description, image);
		// TODO Auto-generated constructor stub
	} 


}
