package org.expu.security;

import org.expu.enums.RoleEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final String adminRole = RoleEnum.ADMINISTRATOR.name();
    private final String auteurRole = RoleEnum.AUTEUR.name();

    
    private final UserDetailsService userDetailsService;


    
    @Autowired
    public WebSecurityConfig(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Bean(name = "passwordEncoder1")
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
        		.antMatchers("/resources/**").permitAll()

//                .antMatchers("/auteur/**").authenticated()
        		.antMatchers("/auteur/**").hasAnyAuthority(auteurRole,adminRole)
                .antMatchers("/admin/**").hasAuthority(adminRole)
                .anyRequest().permitAll()
            .and()
                .formLogin().loginPage("/connexion").defaultSuccessUrl("/index").failureUrl("/connexion?error=true")
                .usernameParameter("username").passwordParameter("password")
            .and()
                .logout().invalidateHttpSession(true)
                .logoutUrl("/logout")
//                .logoutSuccessUrl("/connexion")
                .logoutSuccessUrl("/connexion?logout=true")
            .and()
                .csrf()
            .and()
            	.exceptionHandling().accessDeniedPage("/403")
            .and()
                .sessionManagement().maximumSessions(1).expiredUrl("/connexion");
    }
}