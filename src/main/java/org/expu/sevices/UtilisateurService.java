package org.expu.sevices;


import java.util.Optional;
import org.expu.dao.UtilisateurRepository;
import org.expu.entities.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Service("utilisateurService")
@RestController
public class UtilisateurService implements UserDetailsService {

	private UtilisateurRepository utilisateurRepository;


	@Autowired
	public UtilisateurService(UtilisateurRepository utilisateurRepository) {
		this.utilisateurRepository = utilisateurRepository;
	}



	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Utilisateur utilisateur = utilisateurRepository.findUtilisateurByUserName(username);
	if(username == null) {
			throw new UsernameNotFoundException("il n'existe aucun utilisateur avec le nom : " + username);
		}else {
			return utilisateur;
		}
//		return utilisateur;

	}

	
	// pour gerer le probleme de pagination
		@RequestMapping(value = "/utilisateurs", method = RequestMethod.GET)
		public Page<Utilisateur> listUtilisateur(int page, int size) {
			return utilisateurRepository.findAll(new PageRequest(page, size));
		}

		// pour chercher un Utilisateur
		//http://localhost:8080/chercherUtilisateurs?mc=user&page=0&size=5
		@RequestMapping(value = "/chercherUtilisateurs", method = RequestMethod.GET)
		public Page<Utilisateur> chercher(@RequestParam(name="mc")String mc, @RequestParam(name = "page", defaultValue = "0") int page,
				@RequestParam(name = "size", defaultValue = "5") int size) {
			return utilisateurRepository.chercherUtilisateurs("%" + mc + "%", new PageRequest(page, size));
		}

		@RequestMapping(value = "/utilisateurs/{id}", method = RequestMethod.GET)
		public Optional<Utilisateur> getUtilisateur(@PathVariable("id") Long id) {
			return utilisateurRepository.findById(id);
		}

		@RequestMapping(value = "/utilisateurs", method = RequestMethod.POST)
		public Utilisateur save(@RequestBody Utilisateur u) throws Exception {
			return utilisateurRepository.save(u);
		}

		@RequestMapping(value = "/utilisateurs/{id}", method = RequestMethod.PUT)
		public Utilisateur update(@RequestBody Utilisateur u, @PathVariable Long id) {
			u.setIdUtilisateur(id);
			return utilisateurRepository.saveAndFlush(u);
		}

		@RequestMapping(value = "/utilisateurs/{id}", method = RequestMethod.DELETE)
		public void delete(@PathVariable("id") Long id) {
			utilisateurRepository.deleteById(id);
		}

}
