package org.expu.entities;



import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import lombok.Data;

@Data
@Entity
public class Contact {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long idContact;
		private String email;
		private Date dateContact;  
		private String nom;
		private String sujet;
		private String message;
		private String attribute;
	
		
	private Utilisateur utilisateurContact;
	private Admin adminContact;


	
}
