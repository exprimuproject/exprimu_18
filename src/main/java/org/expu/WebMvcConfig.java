package org.expu;



import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import org.expu.dao.UtilisateurRepository;
import org.expu.entities.Utilisateur;
import org.expu.enums.RoleEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;



@EnableWebMvc
@Configuration
public class WebMvcConfig implements WebMvcConfigurer  {
	 
	private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
			"classpath:/resources/", "classpath:/static/css",
			"classpath:/static/","classpath:/static/templates/" };
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		if (!registry.hasMappingForPattern("/webjars/**")) {
			registry.addResourceHandler("/webjars/**").addResourceLocations(
					"classpath:/META-INF/resources/webjars/");
		}
		if (!registry.hasMappingForPattern("/**")) {
			registry.addResourceHandler("/**").addResourceLocations(
					CLASSPATH_RESOURCE_LOCATIONS);
		}
	}
	
    @Autowired
    public WebMvcConfig(UtilisateurRepository userRepository) {
        // Ceci n'est pas à recopier en production
//        List<RoleEnum> userRole = Collections.singletonList(RoleEnum.USER);
//        List<RoleEnum> adminRole = Arrays.asList(RoleEnum.USER, RoleEnum.ADMINISTRATOR);
//        Utilisateur user = new Utilisateur("user", "user", "User", "USER", userRole);
//        Utilisateur adminUser = new Utilisateur("admin", "admin", "Admin", "ADMIN", adminRole);
//        userRepository.save(user);
//        userRepository.save(adminUser);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("/index");
        registry.addViewController("/connexion").setViewName("connexion");
        registry.addViewController("/auteur").setViewName("auteur/ContenuForm");
        registry.addViewController("/admin").setViewName("admin/admin");
//        registry.addViewController("/admin/listUtilisateur").setViewName("admin/listUtilisateur");
        registry.addViewController("/403").setViewName("403");

    }
    
    @Autowired
    DataSource dataSource;
    
//    @Autowired
//    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
//      auth.jdbcAuthentication().dataSource(dataSource)
////     .usersByUsernameQuery("select username,password, enabled from utilisateur where username=?") 
//     .usersByUsernameQuery("SELECT username , password, '1' as enabled FROM utilisateur WHERE username=? and account_non_expired=1 and account_non_locked=1 and credentials_non_expired=1")
//     .authoritiesByUsernameQuery("SELECT username, role FROM utilisateur INNER JOIN  roles ur ON (utilisateur.id_utilisateur = ur.id_utilisateur ) WHERE utilisateur.username = ?");
//    } 
   
}