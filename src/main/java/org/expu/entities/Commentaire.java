package org.expu.entities;

import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
public class Commentaire {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idCommentaire;
	@Temporal(TemporalType.DATE)
	private Date datePublication;
	private String messageCommentaire;
	

	@ManyToOne
	@JoinColumn(name="fkIdContenu")
		private Contenu contenus;
	
	@ManyToOne
	@JoinColumn(name="fkIdUtilisateur")
		private Utilisateur utilisateurs;
}
