package org.expu.web;

import java.util.List;

import org.expu.dao.ContenuRepository;
import org.expu.dao.UtilisateurRepository;
import org.expu.entities.Contenu;
import org.expu.entities.Utilisateur;
import org.expu.sevices.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdminController {

	@Autowired
	private UtilisateurRepository utilisateurRepository; 
	
	@Autowired
	private ContenuRepository contenuRepository;

	@RequestMapping("/listeActualite")
    public String pageListActualite() {
        return "admin/listeActualite";
    }
	

	@RequestMapping("/listeUtilisateur")
    public String pageFiche(Model model,@RequestParam(name="page", defaultValue="0") int p) {
		Page<Utilisateur> utilisateurs = utilisateurRepository.findAll(new PageRequest(p, 10));
		int pageCount = utilisateurs.getTotalPages();
		int[] pages = new int[pageCount];
		for (int i=0;i<pageCount;i++) pages[i]=i;
		model.addAttribute("pages",pages);
		model.addAttribute("pageUtilisateurs", utilisateurs);

        return "admin/listeUtilisateur";
    }
	
	@RequestMapping("/listePublication")
    public String pageListPublication(Model model,@RequestParam(name="page", defaultValue="0") int p) {
		Page<Contenu> contenus = contenuRepository.findAll(new PageRequest(p, 10));
		int pageCount = contenus.getTotalPages();
		int[] pages = new int[pageCount];
		for (int i=0;i<pageCount;i++) pages[i]=i;
		model.addAttribute("pages",pages);
		model.addAttribute("pageContenus", contenus);
        return "admin/listePublication";
    }
	
	@RequestMapping("/ajouterActualite")
    public String pageAjouterActualite() {
        return "admin/ajouterActualite";
    }
	
	@RequestMapping("/modifierActualite")
    public String pageModifierActualite() {
        return "admin/modifierActualite";
    }
	
//	@RequestMapping("/modifierContenu")
//    public String pageModifierContenu() {
//        return "admin/modifierContenu";
//    }

}
