package org.expu.entities;

import java.util.*;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
public class Actualite {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long idActualite;
		private String titre;
		private String description;
		private Date datePublication;
		private String visible;
		private String miniatureAc;
	
		@ManyToMany
		private Collection<Admin> listAdmin;
		
		@OneToOne(mappedBy="actuRessource")
		private Ressource ressources; 
		
		@OneToMany(mappedBy="actualites", fetch=FetchType.LAZY)
		private Collection<Contenu> listCont; 

}
