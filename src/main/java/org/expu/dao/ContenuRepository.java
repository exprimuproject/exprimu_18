package org.expu.dao;

 
import java.util.List;


import org.springframework.data.repository.query.Param;
import org.expu.entities.Contenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface ContenuRepository extends JpaRepository<Contenu, Long>  {

	 @Query("select c from Contenu c where c.titre like :x ")
	public List<Contenu> findByMc(@Param("x")String motCle);
	 
	 Contenu findByIdContenu(long idContenu);
	 
	 @Query("select c from Contenu c where visiblite like 'Public'")
	 List<Contenu> findAllContenu();
}
