package org.expu.dao;

import org.expu.entities.Actualite;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ActualiteRepository  extends JpaRepository<Actualite, Long> {
	
	@Query("select a from Actualite a where  a.titre like :x")
	public Page<Actualite> ChercherActualite(@Param("x") String mc,Pageable pageable);	

}
