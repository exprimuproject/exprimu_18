package org.expu.entities;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.Data;

@Data
@Entity
public class Contenu implements Serializable {
  
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long idContenu;
		private String descriptif;
		private String typeContenu ;
		private String titre;
		private String visiblite ;
		 @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
		private Date dateCreation;
		 @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
		private Date dateModif;
//		private Date dateModif;	
		private String miniature ;
		private int nbVue;
		private int note;
	
	@ManyToMany
		private Collection<Admin> listAdmins;
	
	@ManyToOne
	@JoinColumn(name="fkIdUtilisateur")
	private Utilisateur utilisateur;
	
	@ManyToOne
	@JoinColumn(name="fkIdActualite")
	private Actualite actualites;
	
	@ManyToMany
	private Collection<Ressource> listResou;
	
	@OneToMany(mappedBy="contenus", fetch=FetchType.LAZY)
	private Collection<Commentaire> listCommentaires; 
		
		
	public  Contenu() {
		// TODO Auto-generated method stub
		super();

	}
}
