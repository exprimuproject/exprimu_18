package org.expu.web;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.expu.dao.ContenuRepository;
import org.expu.dao.UtilisateurRepository;
import org.expu.entities.Contenu;
import org.expu.entities.Utilisateur;
import org.expu.enums.RoleEnum;
import org.expu.sevices.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@CrossOrigin("*")
@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
public class RestExprimuPlateforme {
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@Autowired
	private ContenuRepository  contenurepositroy ; 
	
	UtilisateurService UtilisateurService;
	
	
	@Value("${dir.images}")
	private String imageDir;
	
	@GetMapping("/contact")
    public String pageContact(@RequestParam(name="name", required=false, defaultValue="null") String name, Model model) {
		nameAuth(name,model);
        return "contact";
    }
	
	
	@GetMapping("/detailActualite")
    public String pageDetailActualite(@RequestParam(name="name", required=false, defaultValue="null") String name, Model model) {
		nameAuth(name,model);
        return "detailActualite";
    }
	
	@GetMapping("/forgetpassword")
    public String pageForgetPassword(@RequestParam(name="name", required=false, defaultValue="null") String name, Model model) {
		nameAuth(name,model);
        return "forgetpassword";
    }
	
	@RequestMapping(value = {"/index"})
    public String pageIndex(@RequestParam(name="name", required=false, defaultValue="null") String name, Model model) {
		final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null && auth.isAuthenticated() && !(auth instanceof AnonymousAuthenticationToken))
	    { if(auth.getDetails() !=null)
	        model.addAttribute("name", auth.getName());
	        Utilisateur customUser = (Utilisateur)auth.getPrincipal();
	    	Long userId = customUser.getIdUtilisateur();
	      	 System.out.println(auth.getName()+"/  idUtilisateur :"+userId +"/ role:" +auth.getAuthorities());
	    }else {
	    	System.out.println("null");
	    }
	    List<Contenu> contenus = contenurepositroy.findAllContenu(); 
	    model.addAttribute("contenus", contenus);
      return "index";
    }
	
	
	@GetMapping("/inscription")
    public String pageInscription(@RequestParam(name="name", required=false, defaultValue="null") String name, Model model) {
		nameAuth(name,model);
        return "inscription";
    }
		
	
	@GetMapping("/profil")
    public String pageProfil( Long id, Model model) {
		final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
   	 Utilisateur customUser = (Utilisateur)auth.getPrincipal();
	    	Long userId = customUser.getIdUtilisateur();
	    	//System.out.println(userId);
	    	Utilisateur et = utilisateurRepository.findUtilisateurById(userId);
	       	//System.out.println("Id :"+userId + " / firstname :"+et.getFirstname());
	       	model.addAttribute("firstname",et.getFirstname());
	       	model.addAttribute("utilisateur",et);
        return "profil";
    }
	
//	@GetMapping("/publication")
//    public String pagePublication(@RequestParam(name="name", required=false, defaultValue="null") String name, Model model) {
//		nameAuth(name,model);
//        return "publication";
//    }
	
	@RequestMapping("/quisommesnous")
    public String pageQuiSommesNous(@RequestParam(name="name", required=false, defaultValue="null") String name, Model model) {
		nameAuth(name,model);
        return "quisommesnous";
    }
	
    @RequestMapping(value="/inscription", method=RequestMethod.POST)
	public String save(@Valid Utilisateur utilisateur, Model model, HttpServletRequest request, @RequestParam(name="picture")MultipartFile file) throws IllegalStateException, IOException {
    	
    	
		String radiobtnb2=request.getParameter("inlineRadioOptions");
		utilisateur.setRoles(Collections.singletonList(RoleEnum.valueOf(radiobtnb2)));
		utilisateur.setPassword(request.getParameter("password"));
		utilisateur.setDateCreation(new Date());		
		
		
		utilisateur.setPhoto(file.getOriginalFilename());
		
		if (request.getParameter("password").equals(request.getParameter("confirmPassword"))) {
			utilisateurRepository.save(utilisateur);
        }else {
        	return "redirect:/404";
        }
		//System.out.println("utilisateur inscrit :" +utilisateur.toString() );
		
		if(!(file.isEmpty())) {
			
//			utilisateur.setPhoto(file.getOriginalFilename());
			
			file.transferTo(new File(imageDir+utilisateur.getUsername()));
		}
		
		return "index";
	}
    
    

    
    @GetMapping("/modifierProfil")
    public String pageModifierProfil(Long id, Model model) {
    	final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      	 Utilisateur customUser = (Utilisateur)auth.getPrincipal();
   	    	Long userId = customUser.getIdUtilisateur();
   	    	Utilisateur et = utilisateurRepository.findUtilisateurById(userId);
   	       	model.addAttribute("utilisateur",et);
   	       	//System.out.println(et.toString());
        return "modifierProfil";
    }
    
    

	@RequestMapping(value="/getPhoto1",produces=MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getPhoto1(String username) throws FileNotFoundException, IOException {
		File f=new File(imageDir+username);
		return IOUtils.toByteArray(new FileInputStream(f));
		
	}
	
	@RequestMapping("/visioconference")
    public String pageVisioconference() {
        return "visioconference";
    }
	
	    @RequestMapping(value="/updateProfil", method=RequestMethod.POST)
    public String updateProfil(@Valid Utilisateur utilisateur, Model model, 
    		HttpServletRequest request, @RequestParam(name="picture")MultipartFile file) throws IllegalStateException, IOException {
	    	final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	      	 Utilisateur customUser = (Utilisateur)auth.getPrincipal();
	   	    	Long userId = customUser.getIdUtilisateur();
	   	    	Utilisateur et = utilisateurRepository.findUtilisateurById(userId);
	        	utilisateur.setIdUtilisateur(et.getIdUtilisateur());
//	    		utilisateur.setPassword(request.getParameter("password"));
	        	utilisateur.setRoles(et.getRoles());
	    		utilisateur.setPhoto(et.getPhoto());
	    		
	    		
	    		
	    		if(!(file.isEmpty())) {
	    			utilisateur.setPhoto(file.getOriginalFilename());
	    			file.transferTo(new File(imageDir+utilisateur.getUsername()));
	    		}
	    		
	    		utilisateurRepository.save(utilisateur);
	    		
        return "profil";
    }
	    
	    @RequestMapping("/videoconferences")
	    public String pageVideoConference(@RequestParam(name="name", required=false, defaultValue="null") String name, Model model) {
			nameAuth(name,model);
	        return "videoconferences";
	    }
	    
	 	    
	    //pour recuperer le name d'utilisateur courant
 public void nameAuth(@RequestParam(name="name", required=false, defaultValue="null") String name, Model model) {
	 final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null && auth.isAuthenticated() && !(auth instanceof AnonymousAuthenticationToken))
	    { if(auth.getDetails() !=null)
	        model.addAttribute("name", auth.getName());
	        Utilisateur customUser = (Utilisateur)auth.getPrincipal();
	    	Long userId = customUser.getIdUtilisateur();
	      	 System.out.println(auth.getName()+"/  idUtilisateur :"+userId +"/ role:" +auth.getAuthorities());
	    }else {
	    	System.out.println("null");
	    }
 }
 
 @RequestMapping("/404")
 public String pageErreur() {
     return "404";
 }
 
 @RequestMapping(value="/supprimerUtilisateur")
 public String SsupprimerUtilisateur(Long idUtilisateur, Model model) {
	 Utilisateur utilisateur = utilisateurRepository.getOne(idUtilisateur);
	       	model.addAttribute("utilisateur",utilisateur);
	       	utilisateurRepository.delete(utilisateur);
     return "admin/admin";
 }
 
}
