package org.expu.entities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.expu.enums.RoleEnum;
import org.expu.utils.BCryptManagerUtil;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import lombok.Data;

@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Utilisateur implements UserDetails{
	
    private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_utilisateur")
    private Long idUtilisateur;

    @NotNull
    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @NotNull
    @Column(name = "password", nullable = false)
    private String password;
    
    private String confirmPassword;

    @NotNull
    @Column(name = "firstname", nullable = false)
    private String firstname;

    @NotNull
    @Column(name = "lastname", nullable = false)
    private String lastname;

	private String email;
	private Date dateCreation;
//	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@DateTimeFormat(iso=ISO.DATE)
	private Date dateNaissance;
	private String adresse;
	private String description;
	private String photo;
    
    @ElementCollection(targetClass = RoleEnum.class, fetch = FetchType.EAGER)
    @Cascade(value = CascadeType.REMOVE)
    @JoinTable(
            indexes = {@Index(name = "INDEX_USER_ROLE", columnList = "id_utilisateur")},
            name = "roles",
            joinColumns = {@JoinColumn(name = "id_utilisateur")}
    )
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Collection<RoleEnum> roles;

    @Column(name = "account_non_expired")
    private boolean accountNonExpired;

    @Column(name = "account_non_locked")
    private boolean accountNonLocked;

    @Column(name = "credentials_non_expired")
    private boolean credentialsNonExpired;

    @Column(name = "enabled")
    private boolean enabled;
   
	
	@OneToMany(mappedBy="utilisateurs", fetch=FetchType.LAZY)
	private Collection<Commentaire> listCommentaire; 
	
	@ManyToMany
	private Collection<Auteur> listAuteur;
	
	@ManyToMany
	private Collection<Admin> listAdminContact;
	
	@OneToMany(mappedBy="utilisateur", fetch=FetchType.LAZY)
	private Collection<Contenu> listContenu;

	 public Utilisateur() {
	        this.accountNonExpired = true;
	        this.accountNonLocked = true;
	        this.credentialsNonExpired = true;
	        this.enabled = true;
	        this.roles = Collections.singletonList(RoleEnum.USER);
	    }
	 
	 
	 
	  public Utilisateur(String username, String password, String firstname, String lastname, Collection<RoleEnum> roles) {
	        this.username = username;
	        this.password = BCryptManagerUtil.passwordencoder().encode(password);
	        this.firstname = firstname;
	        this.lastname = lastname;
	        this.accountNonExpired = true;
	        this.accountNonLocked = true;
	        this.credentialsNonExpired = true;
	        this.enabled = true;
	        this.roles = roles;
	    }


	    public Utilisateur(String username, String password, String firstname, String lastname, Collection<RoleEnum> roles,
	    		 String email, Date dateCreation,Date dateNaissance, String adresse, String description, String photo) {
	        this.username = username;
	        this.password = BCryptManagerUtil.passwordencoder().encode(password);
	        this.firstname = firstname;
	        this.lastname = lastname;
	        this.accountNonExpired = true;
	        this.accountNonLocked = true;
	        this.credentialsNonExpired = true;
	        this.enabled = true;
	        this.roles = roles;
	        this.dateNaissance = dateNaissance;
	        this.email = email;
	        this.dateCreation = new Date();
	        this.adresse = adresse;
	        this.description = description;
	        this.photo = photo;
	        
	    }
	    
	    public Utilisateur(String username, String password, String firstname, String lastname, Collection<RoleEnum> roles,
	    		 String email, Date dateCreation,Date dateNaissance, String adresse, String description,String photo,
	    		 Collection<Commentaire> listCommentaire, Collection<Auteur> listAuteur,Collection<Admin> listAdminContact) {
	        this.username = username;
	        this.password = BCryptManagerUtil.passwordencoder().encode(password);
	        this.firstname = firstname;
	        this.lastname = lastname;
	        this.accountNonExpired = true;
	        this.accountNonLocked = true;
	        this.credentialsNonExpired = true;
	        this.enabled = true;
	        this.roles = roles;
	        this.email = email;
	        this.dateCreation = new Date();
	        this.dateNaissance = dateNaissance;
	        this.adresse = adresse;
	        this.description = description;
	        this.photo = photo;
	        this.listCommentaire = listCommentaire;
			this.listAuteur = listAuteur;
			this.listAdminContact = listAdminContact;
	    }
	 
	 
	 

	    @Override
	    public Collection<? extends GrantedAuthority> getAuthorities() {
	        String roles = StringUtils.collectionToCommaDelimitedString(getRoles().stream()
	                .map(Enum::name).collect(Collectors.toList()));
	        return AuthorityUtils.commaSeparatedStringToAuthorityList(roles);
	    }
	    
	    

	    public void setPassword(String password) {
	        if (!password.isEmpty()) {
	            this.password = BCryptManagerUtil.passwordencoder().encode(password);
	        }
	    }
	
	
}
