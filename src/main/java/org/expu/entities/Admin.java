package org.expu.entities;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import org.expu.enums.RoleEnum;

import lombok.Data;

@Entity
@PrimaryKeyJoinColumn(name = "idAdmin")
public class Admin extends Utilisateur{

	
	
	public Admin(String username, String password, String firstname, String lastname, Collection<RoleEnum> roles,
			String email, Date dateCreation, Date dateNaissance, String adresse, String description, String image) {
		super(username, password, firstname, lastname, roles, email, dateCreation, dateNaissance, adresse, description, image);
		// TODO Auto-generated constructor stub
	}



	@ManyToMany
	private Collection<Actualite> listActualite;
	
	@ManyToMany
	private Collection<Contenu> listContenus;
	
	@ManyToMany
	private Collection<Utilisateur> listUtil;

}
