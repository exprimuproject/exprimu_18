package org.expu.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
 
 
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.expu.dao.ActualiteRepository;
import org.expu.dao.ContenuRepository;
import org.expu.dao.RessourceRepository;
import org.expu.dao.UtilisateurRepository;
import org.expu.entities.Actualite;
import org.expu.entities.Contenu;
import org.expu.entities.Ressource;
import org.expu.entities.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

 

@Controller
public class ContenuController {
	@Autowired
	private ContenuRepository  contenurepositroy ; 
	@Value("${dir.image}")
	private String 	imagedir ;
	@Autowired
	private RessourceRepository ressourcerepostitory ;
//	@Autowired
//	private ActualiteRepository actualiteRepository;
 
	
 @RequestMapping(value="/ContenuForm",method =RequestMethod.GET)
	public	 String ContenuForm (@RequestParam(name="name", required=false, defaultValue="null") String name,Model model) {
	 nameAuth(name,model);
	 model.addAttribute("contenu", new Contenu());
	 return "ContenuForm";
}
 @RequestMapping(value="/SavePublication",method = RequestMethod.POST)
	public	String save (@RequestParam(name="name", required=false, defaultValue="null") String name, Model model,@Valid Contenu contenu ,@RequestParam(name="picture") MultipartFile file) throws Exception, IOException {
	// contenu.setUtilisateur();
	Utilisateur u = null ; 
	u = nameAuth(name,model);
	//System.out.println("u" +u.getUsername());
	model.addAttribute("nameu", u.getUsername());
	contenu.setUtilisateur(u);
	contenu.setMiniature(file.getOriginalFilename());

	contenurepositroy.save(contenu);
	
//	Actualite actualite = new Actualite();
//	actualite.setDatePublication(contenu.getDateCreation());
//	actualite.setDescription(contenu.getDescriptif());
//	actualite.setTitre(contenu.getTitre());
//	actualite.setVisible(contenu.getVisiblite());
//	actualite.setMiniatureAc(contenu.getMiniature());
//	actualiteRepository.save(actualite);
	
	 if(!file.isEmpty())
	 {
		 file.transferTo(new File(imagedir+contenu.getIdContenu()));
		 //file.transferTo ( new File(imagedir+contenu.getIdContenu()));
	 }
	 
	 return "redirect:/publication";
}
 @RequestMapping(value="/publication" )
	public	 String consultpublication (Model model , @RequestParam(name="motCle",defaultValue="" )String motCle ) {
	  
	//  List<Contenu> contenus = contenurepositroy.findAll(); 

	  List<Contenu> contenus=contenurepositroy.findByMc("%"+motCle+"%");
	  model.addAttribute("motCle",motCle); 
	 model.addAttribute("contenus", contenus);
	 //model.addAttribute("contenus", contenus.get(contenus.));
	 return "publication";
}

 
 
// @ModelAttribute("listResou")
//	public List<Ressource> intializeRessource(){
//		List<Ressource> ressources = ressourcerepostitory.findAll();
//		return ressources;
//	}
 @ModelAttribute("listResou")
	public List<String> intializeRessource(){
		List<String> ressources = new  ArrayList<String>();
		ressources.add("Mathémtiques");
		ressources.add("Gestion");
		ressources.add("Marketing");
		ressources.add("Sciences Physiques ");
		ressources.add("Commerces");
		ressources.add("Businees & Management");
		ressources.add("Finance");
		return ressources ;
		
	}
 @ModelAttribute("visibilites")
	public List<String> intializeVisibilite(){
		List<String> visibilites = new  ArrayList<String>();
		visibilites.add("Public");
		visibilites.add("Privée");
		return visibilites ;
		
	}
 @ModelAttribute("types")
	public List<String> intializetype(){
		List<String> types = new  ArrayList<String>();
		types.add("Cours");
		types.add("Vidéo conférence");
		types.add("Vidéo");
		
		return types ;
		
	}
 
 @RequestMapping(value="/getPhoto" ,produces=MediaType.IMAGE_JPEG_VALUE)
 @ResponseBody
 public byte[] getphoto(long id) throws Exception, IOException
 {
	 File f = new File(imagedir+id) ;
	 return IOUtils.toByteArray(new FileInputStream(f));
			 
 }
 
 
 
 @RequestMapping(value="/getMoreDetail")
 public String getMoreDetail(@RequestParam long id , Model model ) throws Exception, IOException
 {   
	Contenu contenu = contenurepositroy.findByIdContenu(id);
	 model.addAttribute("contenu", contenu);
	return  "detailContenu" ;
			 
 }
 
 @GetMapping("/detailContenu")
 public String pageDetailContenu(Model model) {
	 //model.addAttribute("contenu",new Contenu());
     return "detailContenu";
 }
 
 
 public Utilisateur nameAuth(@RequestParam(name="name", required=false, defaultValue="null") String name, Model model) {
final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
Utilisateur customUser=null ;    
if (auth != null && auth.isAuthenticated() && !(auth instanceof AnonymousAuthenticationToken))
    { if(auth.getDetails() !=null)
        model.addAttribute("name", auth.getName());
        customUser = (Utilisateur)auth.getPrincipal();
    Long userId = customUser.getIdUtilisateur();
      System.out.println(auth.getName()+" /  idUtilisateur :"+userId +"/ role:" +auth.getAuthorities());
    }else {
    System.out.println("null");
    }
    return customUser;
    } 
 
 @RequestMapping(value="/modifierContenu")
 public String modifierContenu(Long idContenu, Model model) {
	    	Contenu contenu = contenurepositroy.getOne(idContenu);
	       	model.addAttribute("contenu",contenu);
     return "admin/modifierContenu";
 }
 
 @RequestMapping(value="/updatePublication", method=RequestMethod.POST)
public String updatePublication(@Valid Contenu contenu, Model model, 
		HttpServletRequest request, @RequestParam(name="picture")MultipartFile file, @RequestParam(name="id") long idContenu, @RequestParam(name="utilisateurs") long utilisateur) throws IllegalStateException, IOException {
	 	System.out.println(" ----- : " +idContenu);
	    //contenu.setIdContenu(idContenu);
	 	Contenu contenus = contenurepositroy.findByIdContenu(idContenu);
	 	contenu.setIdContenu(idContenu);
	    contenu.setDateModif(new Date());
	    Utilisateur et = new Utilisateur();
	    et.setIdUtilisateur(utilisateur);
     	//contenu.setUtilisateur(et);
	    contenu.setUtilisateur(contenus.getUtilisateur());
     	contenu.setMiniature(contenus.getMiniature());
 		
 		
 		if(!(file.isEmpty())) {
 			contenu.setMiniature(file.getOriginalFilename());
 			file.transferTo(new File(imagedir+contenu.getIdContenu()));
 		}
 		
 		contenurepositroy.save(contenu);
 		
 return "admin/admin";
}
 
 @RequestMapping(value="/supprimerContenu")
 public String SupprimerContenu(Long idContenu, Model model) {
	    	Contenu contenu = contenurepositroy.getOne(idContenu);
	       	model.addAttribute("contenu",contenu);
	       	contenurepositroy.delete(contenu);
     return "admin/admin";
 }

}
