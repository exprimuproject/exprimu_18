package org.expu.sevices;

import java.util.Optional;

import org.expu.dao.ActualiteRepository;
import org.expu.dao.UtilisateurRepository;
import org.expu.entities.Actualite;
import org.expu.entities.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Service
@RestController
public class ActualiteService {

	private ActualiteRepository actualiteRepository;
	
	@Autowired
	public ActualiteService(ActualiteRepository actualiteRepository) {
		this.actualiteRepository = actualiteRepository;
	}
	
	
	
	// pour gerer le probleme de pagination
			@RequestMapping(value = "/actualites", method = RequestMethod.GET)
			public Page<Actualite> listActualite(int page, int size) {
				return actualiteRepository.findAll(new PageRequest(page, size));
			}

			// pour chercher une Actualite
			//http://localhost:8080/chercherActualites?mc=user&page=0&size=10
			@RequestMapping(value = "/chercherActualites", method = RequestMethod.GET)
			public Page<Actualite> chercher(@RequestParam(name="mc")String mc, @RequestParam(name = "page", defaultValue = "0") int page,
					@RequestParam(name = "size", defaultValue = "10") int size) {
				return actualiteRepository.ChercherActualite("%" + mc + "%", new PageRequest(page, size));
			}

			@RequestMapping(value = "/actualites/{id}", method = RequestMethod.GET)
			public Optional<Actualite> getActualite(@PathVariable("id") Long id) {
				return actualiteRepository.findById(id);
			}

			@RequestMapping(value = "/actualites", method = RequestMethod.POST)
			public Actualite save(@RequestBody Actualite a) throws Exception {
				return actualiteRepository.save(a);
			}

			@RequestMapping(value = "/actualites/{id}", method = RequestMethod.PUT)
			public Actualite update(@RequestBody Actualite a, @PathVariable Long id) {
				a.setIdActualite(id);
				return actualiteRepository.saveAndFlush(a);
			}

			@RequestMapping(value = "/actualites/{id}", method = RequestMethod.DELETE)
			public void delete(@PathVariable("id") Long id) {
				actualiteRepository.deleteById(id);
			}

}
